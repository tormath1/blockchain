from client.wallet import Wallet

def test_create_a_wallet_from_existing_keys():
    w = Wallet(
        public_key="a_public_key",
        private_key="a_private_key",
    )
    assert w.public_key == "a_public_key"

def test_create_a_wallet_from_scratch():
    w = Wallet()
    assert w.public_key == "654321"