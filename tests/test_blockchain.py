import pytest

def test_create_new_block(blockchain):
    assert len(blockchain.chain) == 1
    assert blockchain.last_block["proof"] == 100

def test_register_node(blockchain):
    blockchain.register_node("http://localhost:2375")
    assert  "localhost:2375" in blockchain.nodes

def test_register_node_without_http(blockchain):
    with pytest.raises(Exception) as e:
        blockchain.register_node("wow//localhost:2375")
    assert e.value.args[0] == "Must supply an http or https address"

def test_new_transaction(blockchain):
    blockchain.new_transaction(
        sender="sender1",
        recipient="recipient1",
        amount="1"
    )
    expected_transaction = {
        "sender": "sender1",
        "recipient": "recipient1",
        "amount": "1",
    }
    assert blockchain.current_transactions[-1] == expected_transaction