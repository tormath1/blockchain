from flask import json

def test_get_chain(client):
    res = client.get("/chain")
    assert json.loads(res.data).get("length") == 1

def test_post_a_new_transaction(client):
    data = {
        "sender": "sender1",
        "recipient": "recipient1",
        "amount": "amount1",
    }
    res = client.post(
        "/transactions/new",
        data=json.dumps(data),
        content_type='application/json',
        follow_redirects=True,
    )
    assert json.loads(res.data).get("message") == "Transaction will be added to Block 2"

def test_post_a_new_transaction_with_bad_field(client):
    data = {
        "sender": "sender1",
        "bad_field": "recipient1",
        "amount": "amount1",
    }
    res = client.post(
        "/transactions/new",
        data=json.dumps(data),
        content_type='application/json',
        follow_redirects=True,
    )
    assert res.status_code == 400

def test_create_a_new_keys_pair(client):
    res = client.get("/create/keys")
    assert "public_key" in  json.loads(res.data)

def test_mine_a_block(client):
    res = client.get("/mine")
    assert json.loads(res.data).get("previous_hash") == 35293