import pytest
from block.block import Blockchain
from server.app import app

@pytest.fixture
def blockchain():
    return Blockchain()

@pytest.fixture
def client():
    return app.test_client()