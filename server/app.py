import os
from uuid import uuid4

import prometheus_client
from flask import Flask, jsonify, request, Response

from block.block import Blockchain
from helpers.middleware import setup_metrics
from client.wallet import Wallet

app = Flask(__name__)
if os.environ.get("PROD"):
    setup_metrics(app)

node_identifier = str(uuid4()).replace("-", "")

blockchain = Blockchain()

@app.route("/create/keys", methods=["GET"])
def create_keys():
    w = Wallet()
    response = {
        "message": "If you forget these keys, you will lose your wallet.",
        "public_key": "{}".format(w.public_key),
        "private_key": "{}".format(w.private_key),
    }
    return jsonify(response), 201

@app.route("/mine", methods=["GET"])
def mine():
    last_block = blockchain.last_block
    last_proof = last_block.get("proof")
    proof = blockchain.proof_of_work(last_proof)

    # We must receive a reward for finding the proof
    # The sender is "0" to signify that this node has mined a new coin
    blockchain.new_transaction(
        sender="0",
        recipient=node_identifier,
        amount=1
    )

    previous_hash = blockchain.hash(last_block)
    block = blockchain.new_block(proof, previous_hash)

    response = {
        "message": "New block forged",
        "index": block.get("index"),
        "transactions": block.get("transactions"),
        "proof": block.get("proof"),
        "previous_hash": block.get("previous_hash"),
    }

    return jsonify(response), 200

@app.route("/transactions/new", methods=["POST"])
def new_transaction():
    values = request.get_json()

    required = ["sender", "recipient", "amount"]
    if not all(k in values for k in required):
        return "Missing values", 400

    index = blockchain.new_transaction(values["sender"], values["recipient"], values["amount"])

    response = {"message": f"Transaction will be added to Block {index}"}
    return jsonify(response), 201

@app.route("/chain", methods=["GET"])
def full_chain():
    response = {
        "chain": blockchain.chain,
        "length": len(blockchain.chain),
    }
    return jsonify(response), 200

@app.route("/nodes/register", methods=["POST"])
def register_nodes():
    values = request.get_json()
    nodes = values.get("nodes")

    if not nodes:
        return "Error: Please supply a valid list of nodes", 400

    for node in nodes:
        blockchain.register_node(node)

    response = {
        "message": "New nodes have been added",
        "total_nodes": list(blockchain.nodes)
    }

    return jsonify(response), 201

@app.route("/nodes/resolve", methods=["GET"])
def consensus():
    replaced = blockchain.resolve_conflicts()

    if replaced:
        response = {
            "meesage": "Our chain was replaced",
            "new_chain": blockchain.chain
        }
    else:
        response  = {
            "message": "Our chain is authoritative",
            "chain": blockchain.chain
        }

    return jsonify(response), 200

CONTENT_TYPE_LATEST = str("text/plain; version=0.0.4; charset=utf-8;")

@app.route("/metrics")
def metrics():
    return Response(
        response=prometheus_client.generate_latest(),
        mimetype=CONTENT_TYPE_LATEST,
    )

if __name__ == "__main__":
    app.run(host="app", port=12800)