import logging

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

class Wallet(object):

    def __init__(self, private_key=None, public_key=None):
        """
        Can create a wallet from existing keys or not
        :param private_key: Private key of wallet
        :param public_key:  Public Key of wallet
        """

        if private_key and public_key:
            self.private_key = private_key
            self.public_key = public_key
        else:
            log.info("Generating ECC keys")
            # TODO: Use an elliptic algorithm in order to create private/public key
            self.private_key = "123456"
            self.public_key = "654321"

    @property
    def private_key(self):
        return self._private_key

    @private_key.setter
    def private_key(self, value):
        self._private_key = value

    @property
    def public_key(self):
        return self._private_key

    @public_key.setter
    def public_key(self, value):
        self._private_key = value

    def _create_keys(self):
        pass