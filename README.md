### Simple blockain application with monitoring

In this Python3.6 application, we use Flask for creating simple API in order to create a new transaction (`/transactions/new`), insert it into a `block` then mine (`/mine`) this block.

We use `consensus algorithm` for solving the trusted chain issue.

### How to deploy this app and monitoring infrastructure

There are thee images: our app, `Prometheus` (2.0) and `Grafana`. All this images are in the same sub-network: `infranet`. First, you need to start `prometheus` and `grafana` for creating our sub-network.

```shell
$ git clone https://gitlab.com/TortueMat/blockchain
$ cd blockchain
$ docker-compose -f config/prometheus/docker-compose-prometheus.yaml up -d
$ docker-compose -f config/prometheus/docker-compose-prometheus.yaml ps
         Name                        Command               State           Ports
-----------------------------------------------------------------------------------------
prometheus_grafana_1      /run.sh                          Up      0.0.0.0:3000->3000/tcp
prometheus_prometheus_1   /bin/prometheus --config.f ...   Up      0.0.0.0:9090->9090/tcp
```

You can check that `infranet` has been created: 

```shell
$ docker network ls
NETWORK ID          NAME                  DRIVER              SCOPE
...
198543b9f144        prometheus_infranet   bridge              local
...
```

Now, we can deploy `blockchain` application. If you can not download the `blockchain` image, you can build it locally on your machine:

```shell
$ docker-compose -f docker-compose-development.yaml build --no-cache
```

Then, you deploy your fresh image: 

```shell
$ docker-compose -f docker-compose-development.yaml up -d
$ docker-compose -f docker-compose-development.yaml logs app
...
app    | WSGI app 0 (mountpoint='') ready in 0 seconds on interpreter 0x55f8d15e7dc0 pid: 6 (default app)
app    | *** uWSGI is running in multiple interpreter mode ***
app    | spawned uWSGI worker 1 (and the only) (pid: 6, cores: 1)
app    | [pid: 6|app: 0|req: 1/1] 172.19.0.2 () {32 vars in 425 bytes} [Sat Dec 16 20:09:11 2017] GET /metrics => generated 1174 bytes in 3 msecs (HTTP/1.1 200) 2 headers in 113 bytes (1 switches on core 0)
app    | [pid: 6|app: 0|req: 2/2] 172.19.0.2 () {32 vars in 425 bytes} [Sat Dec 16 20:09:16 2017] GET /metrics => generated 1174 bytes in 4 msecs (HTTP/1.1 200) 2 headers in 113 bytes (1 switches on core 0)
...
```

Congratulations, application and infrastructure have been deployed ! A force of `Prometheus` is: you can use any application you want, you just have to write a `middleware` part for gettings `/metrics` endpoints.

### Metrics

In this example, there are only 2 metrics: `request_count` and `request_latency`

  * `request_count` will simply count request number for each enpoints
  * `request_latency` will simply compute request time latency for each endpoints

### Accessing Prometheus

Now your eco-system is running, you can go on [localhost](http://localhost:9090/graph) and execute your request (`request_count` or `request_latency`). Click on execute, and you will see your first insights. 
If you do not see anything, just go on [localhost](http://localhost/mine) and refresh the page for generating metrics !

### Accessing Grafana

Grafana is a tool for displaying **beautiful** graphics, with a bunch of possible customisation, etc. Go on [localhost](http://localhost:3000), and log in with `admin/i_dont_git_a_push`.
Add a data source (`prometheus`), save and test it. Now go on your dashboard and click on a graph for selecting data you want to plot ! 

### API

For the Flask application, you can access to API trough `postman` or a web browser (for `GET`:smiley:)

  *  `/chain`: __GET__ Return the `chain` and him `length`

    ```json
    {
        "chain": [
            {
                "index": 1,
                "previous_hash": 1,
                "proof": 100,
                "timestamp": 1513454949.6899476,
                "transactions": []
            }
        "length": 1
    }
    ```
    
  * `/transactions/new`: __POST__ Create a new transaction with. 
  Post body: 
  ```json
  {
      "sender": "toto",
      "recipient": "yolo",
      "amount": 12,
  }
  ```
  
  * `/mine`: __GET__ Mine a block for appending it to the chain.
  ```json
  {
    "index": 6,
    "message": "New block forged",
    "previous_hash": 129859,
    "proof": "f6387bef6a58eae7196a85759ed56a9f37e2f81226e04b1e167e5066eb443262",
    "transactions": [
        {
            "amount": 1,
            "recipient": "52cf0cd22d784084b7401c73ed32b498",
            "sender": "0"
        }
    ]
  }
  ```
  