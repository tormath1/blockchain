FROM python:3.6-alpine

LABEL authors="mathieu.tortuyaux@gmail.com"

COPY requirements.txt /tmp/requirements.txt

RUN set -e; \
	apk add --no-cache --virtual .build-deps \
		gcc \
		libc-dev \
		linux-headers \
	; \
	pip install --requirement /tmp/requirements.txt; \
    apk del .build-deps;

WORKDIR /opt
ENV PYTHONPATH /opt
COPY . .

CMD uwsgi --http :12800 --file server/app.py --callable app

EXPOSE 12800

RUN rm -rf /tmp/* /var/tmp/*