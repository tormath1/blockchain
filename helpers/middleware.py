import time

from flask import request
from prometheus_client import Counter, Histogram

REQUEST_COUNT = Counter(
    "request_count",
    "App Request Count",
    ["app_name", "method", "endpoint", "http_status"]
)

REQUEST_LATENCY = Histogram(
    "request_latency_seconds",
    "Request latency",
    ["app_name", "endpoint"]
)

def start_timer():
    """
    Start request time
    :return:
    """
    request.start_time = time.time()

def stop_timer(response):
    """
    Compute response time
    :param response: <Response> Response object
    :return: response
    """
    if not "metrics" in request.path:
        resp_time = time.time() - request.start_time
        labels = {
            "app_name": "app",
            "endpoint": request.path,
        }
        REQUEST_LATENCY.labels(**labels).observe(resp_time)
    return response

def record_request_data(response):
    """
    Middleware method for printing metrics
    :param response: Response object
    :return: response: Response object
    """
    if not "metrics" in request.path:
        labels = {
            "app_name": "app",
            "method": str(request.method),
            "endpoint": str(request.path),
            "http_status": str(response.status_code),
        }
        REQUEST_COUNT.labels(**labels).inc()
    return response

def setup_metrics(app):
    """
    Turn on metrics for given app
    :param app: Flask application
    :return:
    """
    app.before_request(start_timer)
    app.after_request(record_request_data)
    app.after_request(stop_timer)